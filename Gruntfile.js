'use strict';

module.exports = function(grunt) {
  grunt.initConfig({
    slim: {
      dist: {
        options: {
          pretty: true
        },
        files: {
          'index.html': [
            'templates/header.slim',
            'templates/yeld.slim',
            'templates/footer.slim'
          ]
        }
      }
    },
    sass: {
      dist: {
        options: {
          style: 'compressed'
        },
        files: {
          'style.css': 'css/*.scss',
        }
      }
    },
    autoprefixer: {
      single_file: {
        src: 'style.css',
        dest: 'style-prefixed.css'
      }
    },
    concat: {
      dist: {
        src: [
          'js/*.js',
        ],
        dest: 'build.js'
      },
      uglify: {
        dist: {
          src: 'build.js',
          dest: 'build.min.js'
        }
      }
    },
    watch: {
      options: {
        livereload: true,
        spawn: false
      },
      slim: {
        files: ['templates/*.slim'],
        tasks: ['slim:dist'],
      },
      scripts: {
        files: ['js/*.js'],
        tasks: ['concat', 'uglify'],
      },
      css: {
        files: ['css/*.scss'],
        tasks: ['sass', 'autoprefixer'],
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-shell');

  grunt.loadNpmTasks('grunt-slim');

  grunt.registerTask('default', ['slim', 'concat', 'sass', 'watch', 'autoprefixer', 'uglify:dist']);
};